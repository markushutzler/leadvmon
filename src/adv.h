/*
 * Copyright 2022 - 2022, Markus Hutzler
 * SPDX-License-Identifier: MIT
 *
 * \file adv.h
 *
 * Module for parsing and printing advertisement data.
 */

#include <systemd/sd-bus.h>

typedef struct advertisment {
	int if_n; ///< Interface number. For example 0 for hci0
	uint8_t device[6];
	int16_t rssi;
	uint16_t manuf_id;
	size_t manuf_data_len;
	uint8_t manuf_data[32];
} adv_data_t;

/**
 * Print advertisement data.
 *
 * The format is fix and consists of the device address separated by `:`, the
 * RSSI value in `()` and the manufacturer id and data in `[]`.
 */
int adv_print(adv_data_t *ad);

/**
 * Parse device address.
 *
 * The patch must match the DBus path including the interface path and device
 * address with `_` separation.
 *
 * \return 0 if the device address has been parsed correctly.
 */
int adv_parse_device(adv_data_t *ad, const char *path);

/**
 * Parse RSSI from DBus message.
 *
 * Copy the RSSI value from the DBus message to the advertisement data
 * structure. The DBus message `m` must be pointing to the value of the "RSSI"
 * key in the received message.
 *
 * \return 0 if RSSI was parsed correctly
 */
int adv_parse_rssi(adv_data_t *ad, sd_bus_message *m);

/**
 * Parse manufacturer data from DBus message.
 *
 * Read company id and manufacturer from DBus and store result in advertisement
 * data structure.
 *
 * \return 0 if data was parsed correctly.
 */
int adv_parse_manufacturer_data(adv_data_t *ad, sd_bus_message *m);
