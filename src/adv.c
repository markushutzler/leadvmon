/*
 * Copyright 2022 - 2022, Markus Hutzler
 * SPDX-License-Identifier: MIT
 */

#include "adv.h"

#include <stdio.h>
#include <stdlib.h>

#include <systemd/sd-bus.h>

int adv_print(adv_data_t *ad)
{
	int i;
	printf("%02x:%02x:%02x:%02x:%02x:%02x (%d)", ad->device[0], ad->device[1],
		   ad->device[2], ad->device[3], ad->device[4], ad->device[5],
		   ad->rssi);

	printf(" [");
	if (ad->manuf_data_len) {
		printf("%04x:", ad->manuf_id);
		for (i = 0; i < ad->manuf_data_len; i++) {
			printf("%02x", ad->manuf_data[i]);
		}
	}
	printf("]");
	printf("\n");
	return 0;
}

int adv_parse_device(adv_data_t *ad, const char *path)
{
	int ret = sscanf(path, "/org/bluez/hci%d/dev_%02X_%02X_%02X_%02X_%02X_%02X",
					 &ad->if_n, &ad->device[0], &ad->device[1], &ad->device[2],
					 &ad->device[3], &ad->device[4], &ad->device[5]);
	if (ret != 7) {
		return 1;
	}
	return 0;
}

int adv_parse_rssi(adv_data_t *ad, sd_bus_message *m)
{
	int r;
	r = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT, "n");
	if (r < 0) {
		return 1;
	}
	r = sd_bus_message_read(m, "n", &ad->rssi);

	// Exit container even if read has failed
	sd_bus_message_exit_container(m);

	if (r < 0) {
		return 1;
	}
	return 0;
}

int adv_parse_manufacturer_data(adv_data_t *ad, sd_bus_message *m)
{
	uint8_t data;
	int r, ret = 0;
	r = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT, "a{qv}");
	if (r < 0) {
		return 1;
	}

	r = sd_bus_message_enter_container(m, SD_BUS_TYPE_ARRAY, "{qv}");
	if (r < 0) {
		sd_bus_message_exit_container(m);
		return 1;
	}

	while ((r = sd_bus_message_enter_container(m, SD_BUS_TYPE_DICT_ENTRY,
											   "qv")) > 0) {
		r = sd_bus_message_read(m, "q", &ad->manuf_id);
		if (r < 0) {
			ret = 1;
			break;
		}
		r = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT, "ay");
		if (r < 0) {
			ret = 1;
			sd_bus_message_exit_container(m);
			break;
		}

		ad->manuf_data_len = 0;
		while (r = sd_bus_message_enter_container(m, SD_BUS_TYPE_ARRAY, "y")) {
			r = sd_bus_message_read(m, "y", &data);
			if (r < 0 || ad->manuf_data_len >= sizeof(ad->manuf_data)) {
				ret = 1;
				break;
			}
			ad->manuf_data[ad->manuf_data_len++] = data;
		}
		sd_bus_message_exit_container(m);
		sd_bus_message_exit_container(m);
	}
	sd_bus_message_exit_container(m);
	sd_bus_message_exit_container(m);
	return ret;
}
