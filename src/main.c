/*
 * Copyright 2022 - 2022, Markus Hutzler
 * SPDX-License-Identifier: MIT
 */

#include <stdio.h>
#include <stdlib.h>

#include <systemd/sd-bus.h>

#include "adv.h"

/*
 * Callback for received "PropertiesChanged" signals.
 *
 * \return 1 if signal should not be processed any further, 0 if no processing
 *         is required and -1 if the processing resulted in an error
 */
int on_changed(sd_bus_message *m, void *userdata, sd_bus_error *ret_error)
{
	const char *if_name = NULL;
	const char *path = NULL;
	char *key;
	int r;
	int ret = 0;
	const char *types;
	adv_data_t ad;

	memset(&ad, 0, sizeof(ad));
	if (sd_bus_message_read_basic(m, 's', &if_name) < 0) {
		return -1;
	}

	/* Only process changes from the org.bluez.Device1 interface. */
	if (strcmp(if_name, "org.bluez.Device1") != 0) {
		return 1;
	}
	path = sd_bus_message_get_path(m);
	if (path == NULL) {
		return -1;
	}
	if (adv_parse_device(&ad, path) != 0) {
		return -1;
	}

	// Iterate Key / Value of DBus message
	r = sd_bus_message_enter_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
	if (r < 0) {
		return -1;
	}
	while ((r = sd_bus_message_enter_container(m, SD_BUS_TYPE_DICT_ENTRY,
											   "sv")) > 0) {
		r = sd_bus_message_read(m, "s", &key);
		if (r < 0) {
			ret = -1;
			sd_bus_message_exit_container(m);
			break;
		}
		r = sd_bus_message_peek_type(m, NULL, &types);
		if (r < 0) {
			ret = -1;
			sd_bus_message_exit_container(m);
			break;
		}
		if (strcmp(key, "RSSI") == 0 && strcmp(types, "n") == 0) {
			adv_parse_rssi(&ad, m);
		} else if (strcmp(key, "ManufacturerData") == 0 &&
				   strcmp(types, "a{qv}") == 0) {
			adv_parse_manufacturer_data(&ad, m);
		}
		sd_bus_message_exit_container(m);
	}
	sd_bus_message_exit_container(m);

	adv_print(&ad);
	return ret;
}

int main(void)
{
	int ret = 0;
	sd_bus *bus = NULL;
	sd_bus_message *m = NULL;
	sd_bus_error err = SD_BUS_ERROR_NULL;

	/* Connect to system bus */
	ret = sd_bus_open_system(&bus);
	if (ret < 0) {
		fprintf(stderr, "Failed to attach to system bus: %s\n", strerror(-ret));
		goto cleanup;
	}

	/* Start the discovery */
	ret = sd_bus_call_method(bus, "org.bluez", "/org/bluez/hci0",
							 "org.bluez.Adapter1", "StartDiscovery", &err, &m,
							 NULL);
	if (ret < 0) {
		fprintf(stderr, "Failed to start discovery: %s\n", err.message);
		goto cleanup;
	}

	/* Allow duplicates */
	ret = sd_bus_call_method(bus, "org.bluez", "/org/bluez/hci0",
							 "org.bluez.Adapter1", "SetDiscoveryFilter", &err,
							 &m, "a{sv}", 1, "DuplicateData", "b", 1, NULL);
	if (ret < 0) {
		fprintf(stderr, "Failed to set filter: %s\n", err.message);
		goto cleanup;
	}

	/*
	 * Generate callback if properties change. This will include LE
	 * advertisement messages.
	 */
	ret = sd_bus_add_match(bus, NULL,
						   "interface='org.freedesktop.DBus.Properties',"
						   "member='PropertiesChanged',type='signal'",
						   on_changed, NULL);
	if (ret < 0) {
		fprintf(stderr, "Failed to start monitoring: %s\n", strerror(-ret));
		goto cleanup;
	}

	/* Run event loop for sd-bus */
	while (1) {
		ret = sd_bus_process(bus, NULL);
		if (ret < 0) {
			fprintf(stderr, "Monitor loop failed: %s\n", strerror(-ret));
			goto cleanup;
		}
		ret = sd_bus_wait(bus, -1);
		if (ret < 0) {
			fprintf(stderr, "Monitor loop failed: %s\n", strerror(-ret));
			goto cleanup;
		}
	}

cleanup:
	sd_bus_error_free(&err);
	sd_bus_message_unref(m);
	sd_bus_unref(bus);
	return ret < 0 ? -ret : 0;
}
