BLE Advertisement Monitor
=========================

A simple advertising monitor for Bluetooth LE advertisement packages. This
project is only used as an example for using sd-bus and bluez with LE devices.

Feel free to modify the code to fit the desired format.

Building from source
--------------------

This project uses CMake to build.

The following packages are required:

 * `gcc`
 * `cmake`
 * `libsystemd-dev`

~~~sh
mkdir build && cd build
cmake ..
make
~~~

Contributing
------------

Tools are used to keep this repository clean. Use clang-format version 11+ to
format the code before committing. Additionally, it is recommended to use
`cppcheck` to find possible issues.

The following additional packages are required:

 * `clang-format`
 * `cppcheck`

~~~sh
clang-format -i src/*.c
clang-format -i src/*.h
mkdir build && cd build
cmake .. -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
cppcheck --project=compile_commands.json \
    --enable=all --suppress=missingIncludeSystem
~~~
